package br.edu.up.playermp3;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.util.Random;

public class MainActivity extends AppCompatActivity
    implements MediaPlayer.OnCompletionListener,
    SeekBar.OnTouchListener {

  ImageView btnTocar;
  ImageView btnSequencia;
  ImageView imgCapa;
  SeekBar barraDeProgresso;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnTocar = (ImageView) findViewById(R.id.btnTocar);
    btnSequencia = (ImageView) findViewById(R.id.btnSequencia);
    imgCapa = (ImageView) findViewById(R.id.imgCapa);
    barraDeProgresso = (SeekBar) findViewById(R.id.barraDeProgresso);
    barraDeProgresso.setOnTouchListener(this);

  }

  public void onClickVoltar(View v){

    if(tocarAleatorio){
      Random r = new Random();
      musicaAtual = r.nextInt(musicas.length -1);
    } else {
      musicaAtual = musicaAtual - 1;
      if (musicaAtual < 0) {
        musicaAtual = musicas.length - 1;
      }
    }
    parar();
    tocar();

  }

  MediaPlayer player;

  int[] musicas = {
      R.raw.anitta_bang,
      R.raw.jota_quest_na_moral,
      R.raw.pink_floyd_another_brick_in_the_wall_p2,
      R.raw.sambo_sunday_bloody_sunday,
      R.raw.talking_heads_psycho_killer,
      R.raw.the_doors_light_my_fire,
      R.raw.zeca_baleiro_disritmia
  };

  int[] capas = {
      R.drawable.anitta,
      R.drawable.jota_quest,
      R.drawable.pink_floyd,
      R.drawable.sambo,
      R.drawable.talking_heads,
      R.drawable.the_doors,
      R.drawable.zeca_baleiro
  };

  int musicaAtual = 5;


  public void onClickTocar(View v){

    if (player == null) {
      tocar();
    } else if (!player.isPlaying()){
      player.start();
      btnTocar.setImageResource(R.drawable.pause50px);
    } else {
      player.pause();
      btnTocar.setImageResource(R.drawable.play50px);
    }

  }

  public void tocar(){
    imgCapa.setImageResource(capas[musicaAtual]);
    player = MediaPlayer.create(this, musicas[musicaAtual]);
    player.setOnCompletionListener(this);
    player.start();
    btnTocar.setImageResource(R.drawable.pause50px);

    barraDeProgresso.setMax(player.getDuration());

    atualizarBarra();
  }

  public void atualizarBarra(){
    if(player != null && player.isPlaying()){
      barraDeProgresso.setProgress(player.getCurrentPosition());


      Runnable processo = new Runnable() {
        @Override
        public void run() {
          atualizarBarra();
        }
      };
      new Handler().postDelayed(processo, 1000);

    }
  }

  public void onClickParar(View v){
    parar();
  }

  public void parar(){
    if (player != null && player.isPlaying()){
      player.stop();
      player.release();
      player = null;
      btnTocar.setImageResource(R.drawable.play50px);
    }
  }

  public void onClickAvancar(View v){

    if(tocarAleatorio){
      Random r = new Random();
      musicaAtual = r.nextInt(musicas.length -1);
    } else {
      musicaAtual = musicaAtual + 1;
      if (musicaAtual > musicas.length - 1) {
        musicaAtual = 0;
      }
    }
    parar();
    tocar();
  }

  boolean tocarAleatorio = false;

  public void onClickSequencia(View v){

    if(tocarAleatorio){
      tocarAleatorio = false;
      btnSequencia.setImageResource(R.drawable.sequencial50px);
    } else{
      tocarAleatorio = true;
      btnSequencia.setImageResource(R.drawable.random50px);
    }

  }

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    if(tocarAleatorio){
      Random r = new Random();
      musicaAtual = r.nextInt(musicas.length -1);
    } else {
      musicaAtual = musicaAtual + 1;
      if (musicaAtual > musicas.length - 1) {
        musicaAtual = 0;
      }
    }
    tocar();
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {

    player.seekTo(barraDeProgresso.getProgress());
    return false;
  }
}
